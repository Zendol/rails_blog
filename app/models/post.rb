class Post < ApplicationRecord
  belongs_to :category
  has_many :comments, dependent: :destroy
  scope :ordered, -> { order(id: :desc)}
  scope :with_categories, -> {includes(:category)}
end
